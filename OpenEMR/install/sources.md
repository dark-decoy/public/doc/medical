# OpenEMR Install Sources

* [OpenEMR with nginx and php\-fpm \- OpenEMR Project Wiki](https://www.open-emr.org/wiki/index.php/OpenEMR_with_nginx_and_php-fpm "OpenEMR with nginx and php-fpm - OpenEMR Project Wiki")
* [OpenEMR Project Wiki](https://www.open-emr.org/wiki/index.php/OpenEMR_Wiki_Home_Page "OpenEMR Project Wiki")
* [OpenEMR Installation Guides \- OpenEMR Project Wiki](https://www.open-emr.org/wiki/index.php/OpenEMR_Installation_Guides "OpenEMR Installation Guides - OpenEMR Project Wiki")
* [OpenEMR 7 New Clinic Essentials Setup \- Management and Administration \- OpenEMR Community](https://community.open-emr.org/t/openemr-7-new-clinic-essentials-setup/21942 "OpenEMR 7 New Clinic Essentials Setup - Management and Administration - OpenEMR Community")
* [OpenEMR 7 New Clinic Setup \- OpenEMR Project Wiki](https://www.open-emr.org/wiki/index.php/OpenEMR_7_New_Clinic_Setup "OpenEMR 7 New Clinic Setup - OpenEMR Project Wiki")
