# OpenEMR Sources

* [openemr\/openemr\-devops\: OpenEMR administration and deployment tooling](https://github.com/openemr/openemr-devops "openemr/openemr-devops: OpenEMR administration and deployment tooling")
* [openemr\/tests\/e2e at master · openemr\/openemr](https://github.com/openemr/openemr/tree/master/tests/e2e "openemr/tests/e2e at master · openemr/openemr")
* [Setting up your own test automation environment \- Learn web development \| MDN](https://developer.mozilla.org/en-US/docs/Learn/Tools_and_testing/Cross_browser_testing/Your_own_automation_environment "Setting up your own test automation environment - Learn web development | MDN")
* [Selenium](https://www.selenium.dev/ "Selenium")
* [Icd 2022 external data load by stephenwaite · Pull Request \#4628 · openemr\/openemr](https://github.com/openemr/openemr/pull/4628/commits/94052a1c339cf0b81883d3aa8b4cfa18e31809fb "Icd 2022 external data load by stephenwaite · Pull Request #4628 · openemr/openemr")
* [https\:\/\/github\.com\/RedHatOfficial\/openemr\-kube](https://github.com/RedHatOfficial/openemr-kube "https://github.com/RedHatOfficial/openemr-kube")
* [ICD10 Install Problem OEMR7 \- Support \- OpenEMR Community](https://community.open-emr.org/t/icd10-install-problem-oemr7/19421/14 "ICD10 Install Problem OEMR7 - Support - OpenEMR Community")
* [Steps for a patch release \- OpenEMR Project Wiki](https://www.open-emr.org/wiki/index.php/Steps_for_a_patch_release "Steps for a patch release - OpenEMR Project Wiki")
