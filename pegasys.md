# Pegasys Fixes

## Patient Transfer Disk Full Error When Disk is not Full

Open a UNIX Window (right click sys util then sys maint then Unix Window)

Become superuser

su
adacsu

df -k   will show % before we start​​
 cd /files/pat
mkdir Peg_filler
 cd Peg_filler
mkfile 500000k  .filler_file_1

This should keep the used space above the 14% boundary, so that the problem
does not re-occur when files are deleted. If it does not push the usage up high enough
do again using .filler_file_2 ect... unitl 15-16%

· adac:# df -k will show % after files are added​

[Back](./readme.md)